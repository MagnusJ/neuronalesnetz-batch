import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Random;

import NeuronalesNetz.NeuronalesNetz;


public class main {

	public static void main(String[] args) {
		//konstanten---------------------------------------------------------------------
			double lernrate=0.0129;
			int outFaktor=1;
			
			int[] layerThick=new int[4];
			layerThick[0]=256;
			layerThick[1]=4;
			layerThick[2]=2;
			layerThick[3]=10;
			
			Random rand = new Random();
			
			String fileName="TEST-006";
			
			int FEHLER;
			double[] test=new double[256];
			double[] exp=new double[10];
		//-konstanten--------------------------------------------------------------------
	
		//Initialisierung
			NeuronalesNetz neuesNetz=new NeuronalesNetz(layerThick,lernrate,outFaktor);
			//NeuronalesNetz neuesNetz=new NeuronalesNetz(fileName,lernrate);
			//neuesNetz.read(fileName);
		//-Initialisierung
	
		//Testdaten----------------------------------------------------------------------
			
		//pr�fen			
			for(int i=0;i<test.length;i++){
				test[i]=0;
			}
			test[37]=1;
			test[38]=1;
			test[39]=1;
			test[52]=1;
			test[67]=1;
			test[84]=1;
			test[56]=1;
			test[73]=1;
			test[88]=1;
			test[101]=1;
			test[102]=1;
			test[103]=1;
			test[116]=1;
			test[120]=1;
			test[131]=1;
			test[147]=1;
			test[164]=1;
			test[137]=1;
			test[153]=1;
			test[169]=1;
			test[181]=1;
			test[182]=1;
			test[183]=1;
			test[184]=1;
			test[20]=1;
			test[21]=1;
			test[22]=1;
			test[23]=1;
			test[24]=1;
			test[25]=1;
			test[40]=1;
			test[41]=1;
			test[42]=1;
			//test[40]=1;
			//test[40+16]=1;
			//test[40+16+16]=1;
			//test[40+16+16+16]=1;
			//test[40+16+16+16+16]=1;
			//test[40+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16+16+16+16]=1;
			
			
			for(int i =0;i<10;i++){
				System.out.println(i+":  "+Math.round(neuesNetz.calculate(test)[i]));
			}
		//-pr�fen
		
		//-Testdaten---------------------------------------------------------------------

			
			
		//Einlesen---------------------------------------------------------------------------
			String datenString[][]=new String[1593][266];	//0 bis 255 Pixel 256-265 Expectet
			double[][] datenDouble=new double[1593][266];
			BufferedReader bufferedReader = null; 
			File file = new File("Trainingsdaten/data.txt"); 
			try { 
				bufferedReader = new BufferedReader(new FileReader(file)); 
				String line; 
				int i=0;
				while (null != (line = bufferedReader.readLine())) { 
					datenString[i]=line.split("~");    
					for(int k=0;k<datenString[i].length;k++){
						datenDouble[i][k]=Double.valueOf(datenString[i][k]);
					}
					i++;
				} 
			} catch (IOException e) { 
				e.printStackTrace(); 
			}
			finally { 
				if (null != bufferedReader) { 
					try { 
						bufferedReader.close(); 
					} 
					catch (IOException e) { 
						e.printStackTrace(); 
					} 
				} 
			} 
		//-Einlesen--------------------------------------------------------------------------
			
			
			
			
		//lernen
			int a;
			for(int k=0;k<800000;k++){
				a = (int) Math.round(rand.nextDouble()*(datenDouble.length-1));
				
				for(int i=0;i<test.length;i++){
					test[i]=datenDouble[a][i];
				}
				for(int i=test.length;i<datenDouble[a].length;i++){	
					if(datenDouble[a][i]==0){
						exp[i-test.length]=-1;
					}
					else{
						exp[i-test.length]=1;
					}
				}
			
				neuesNetz.learn(test, exp);
			}
		//-lernen
			
		//pr�fen
			FEHLER=0;
			double[] ergebnis=new double[exp.length];
			for(int k=0;k<datenDouble.length;k++){
				for(int i=0;i<test.length;i++){
					test[i]=datenDouble[k][i];
				}
				for(int i=test.length;i<datenDouble[k].length;i++){
					if(datenDouble[k][i]==0){
						exp[i-test.length]=-1;
					}
					else{
						exp[i-test.length]=1;
					}
				}
			
				ergebnis=neuesNetz.calculate(test);
				for(int i=0;i<exp.length;i++){
					if(Math.sqrt(ergebnis[i]*ergebnis[i])-Math.sqrt(exp[i]*exp[i])>0.5){
						FEHLER++;
						System.out.println(FEHLER);
						break;
					}
				}
			}
			System.out.println("Anzahl an Falschen: "+FEHLER);
		//-pr�fen
		
		//save
			neuesNetz.save(fileName);		
		//-save
			
			
			for(int i=0;i<test.length;i++){
				test[i]=0;
			}
			test[37]=1;
			test[38]=1;
			test[39]=1;
			test[52]=1;
			test[67]=1;
			test[84]=1;
			test[56]=1;
			test[73]=1;
			test[88]=1;
			test[101]=1;
			test[102]=1;
			test[103]=1;
			test[116]=1;
			test[120]=1;
			test[131]=1;
			test[147]=1;
			test[164]=1;
			test[137]=1;
			test[153]=1;
			test[169]=1;
			test[181]=1;
			test[182]=1;
			test[183]=1;
			test[184]=1;
			test[20]=1;
			test[21]=1;
			test[22]=1;
			test[23]=1;
			test[24]=1;
			test[25]=1;
			test[40]=1;
			test[41]=1;
			test[42]=1;
			//test[40]=1;
			//test[40+16]=1;
			//test[40+16+16]=1;
			//test[40+16+16+16]=1;
			//test[40+16+16+16+16]=1;
			//test[40+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16+16+16]=1;
			//test[40+16+16+16+16+16+16+16+16+16+16+16]=1;
			
			
			for(int i =0;i<10;i++){
				System.out.println(i+":  "+Math.round(neuesNetz.calculate(test)[i]));
			}
	}
}



