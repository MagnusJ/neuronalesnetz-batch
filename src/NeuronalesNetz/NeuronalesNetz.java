package NeuronalesNetz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class NeuronalesNetz {
	private Layer layer[];
	private double input[];
	private int layerThickness[];
	private double lernFaktor;
	private double[] mistake;
	private double[] mistakeh;
	private boolean richtig;
	public int lernzyklusZ�hler;
	public int outFaktor;
	
	public NeuronalesNetz(int[] layerThickness, double lernFaktor,int outFaktor) {
		this.lernFaktor=lernFaktor;
		this.layerThickness=layerThickness;
		this.outFaktor=outFaktor;
		this.layerThickness[this.layerThickness.length-1]=this.layerThickness[this.layerThickness.length-1]*this.outFaktor;
		this.layer=new Layer[this.layerThickness.length-1];
		
		for(int i=1;i<this.layerThickness.length;i++){
			this.layer[i-1]=new Layer(this.layerThickness[i],this.layerThickness[i-1]);
		}
		//System.out.println(layerThickness[3]);
		
		double[] input = new double[this.layerThickness[0]];
		for(int i=0;i<this.layerThickness[0];i++){
			input[i]=1;
		}
		this.calculate(input);
	}
	
	public NeuronalesNetz(int[] layerThickness, double lernFaktor){
		this.lernFaktor=lernFaktor;
		this.layerThickness=layerThickness;
		this.outFaktor=1;
		this.layerThickness[this.layerThickness.length-1]=this.layerThickness[this.layerThickness.length-1]*this.outFaktor;
		this.layer=new Layer[this.layerThickness.length-1];
		
		for(int i=1;i<this.layerThickness.length;i++){
			this.layer[i-1]=new Layer(this.layerThickness[i],this.layerThickness[i-1]);
		}
		//System.out.println(layerThickness[3]);
		
		double[] input = new double[this.layerThickness[0]];
		for(int i=0;i<this.layerThickness[0];i++){
			input[i]=1;
		}
		this.calculate(input);
	}
	
	public NeuronalesNetz(String fileName, double lernFaktor){
		this.lernFaktor=lernFaktor;
		this.outFaktor=1;
		
		
		//double readData[][][];
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(fileName+".NN"));
			double readData[][][] = (double[][][])inputStream.readObject();
			for(int i=0;i<readData.length;i++){
				for(int j=0;j<readData[i].length;j++){
					for(int k=0;k<readData[i][j].length-1;k++){
						//System.out.print(readData[i][j][k]);
					}	
				}	
			}
			int layerThicknessTest[]=new int[readData.length+1];
			for(int i=0;i<readData.length;i++){
				layerThicknessTest[i]=readData[i].length-1;
				//System.out.println("i:   "+(readData[i].length-1));
			}
			layerThicknessTest[layerThicknessTest.length-1]=readData[readData.length-1][0].length;
			
			
			
			this.layerThickness=layerThicknessTest;
			this.layerThickness[this.layerThickness.length-1]=this.layerThickness[this.layerThickness.length-1]*this.outFaktor;
			this.layer=new Layer[this.layerThickness.length-1];
			for(int i=1;i<this.layerThickness.length;i++){
				this.layer[i-1]=new Layer(this.layerThickness[i],this.layerThickness[i-1]);
			}
			for(int i=0;i<this.layerThickness.length-1;i++){
					layer[i].load(readData[i]);
			}
			
		} catch (IOException e) {
			System.err.println( "Error reading file!" );
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double[] input = new double[this.layerThickness[0]];
		for(int i=0;i<this.layerThickness[0];i++){
			input[i]=1;
		}
		this.calculate(input);
		
	}

	public double[] calculate(double input[]){
		double[] help;
		double[] output;
		
		this.input=new double[this.layerThickness[1]];
		this.input=this.layer[0].calculate(input);
		for(int i=1;i<this.layerThickness.length-1;i++){
			help = new double[this.layerThickness[i+1]];
			help=this.layer[i].calculate(this.input);
			this.input=new double[this.layerThickness[i+1]];
			this.input=help;
		}
		help=this.input;
		
		output = new double[this.layerThickness[this.layerThickness.length-1]/this.outFaktor];
		int k=0;
		for(int i=0;i<this.layerThickness[this.layerThickness.length-1];i=i+this.outFaktor){
			double a=-1;
			for(int j=0;j<this.outFaktor;j++){
				if(a<this.input[i+j]){
					a=this.input[i+j];
				}
			}
			output[k]=a;
			k++;
		}
			
		return output; //output des NN
	}
	
	public boolean learn(double[] input, double[] expected){
		this.calculate(input);
		
		this.lernzyklusZ�hler++;
		
		this.mistake=new double[this.layerThickness[layerThickness.length-1]];
		//System.out.println(this.mistake.length);
		//System.out.println();
		
		for(int i=0;i<this.mistake.length;i++){
		//	this.mistake[i]=(this.input[i]-expected[i])*this.lernFaktor;
		}
		
		int j=0;
		for(int i=0;i<this.mistake.length;i=i+this.outFaktor){
			//System.out.println(i+1);
			//System.out.println();
			//System.out.println();
			if(expected[j]==1){
				double a = -1;
				int maxPos = 0;
				for(int k=0;k<this.outFaktor;k++){
					if(a<this.input[i+k]){
						a=this.input[i+k];
						maxPos=k;
					}
				}
				for(int k=0;k<this.outFaktor;k++){
					this.mistake[i+k]=0;
				}
				this.mistake[i+maxPos]=(this.input[i+maxPos]-expected[j])*this.lernFaktor;
			}
			else{
				for(int k=0;k<this.outFaktor;k++){
					this.mistake[i+k]=(this.input[i+k]-expected[j])*this.lernFaktor;
				}
			}
			
			j++;
		}
				
		this.richtig=true;
		for(int i=0;i<this.mistake.length;i++){
			if(this.mistake[i]/this.lernFaktor>=1){
				this.richtig=false;
			}
		}
		
		for(int i=this.layerThickness.length-1;i>0;i--){
			this.mistakeh=new double[this.layerThickness[i]];
			this.mistakeh=this.layer[i-1].learn(this.mistake);
			this.mistake=new double[this.layerThickness[i]];
			this.mistake=this.mistakeh;
		}
		
		
		return this.richtig;
	}
	
	public void show(double in[], double exp[]){
		System.out.println();
		System.out.print("input:   ");
		for(int i=0;i<this.layerThickness[0];i++){
			System.out.print(in[i]+"     ");
		}
		System.out.println();
		this.calculate(in);
		System.out.print("Ist:     ");
		for(int i=0;i<this.layerThickness[this.layerThickness.length-1];i++){
			System.out.print(this.input[i]+"     ");
		}
		System.out.println();
		System.out.print("Soll:    ");
		for(int i=0;i<this.layerThickness[this.layerThickness.length-1];i++){
			System.out.print(exp[i]+"     ");
		}
		System.out.println();
		System.out.print("Fehler:  ");
		for(int i=0;i<this.layerThickness[this.layerThickness.length-1];i++){
			System.out.print(Math.abs(Math.round((this.input[i]-exp[i])*10)/10.0)+"   ");
		}
		System.out.println();
		System.out.println("Lernzyklusz�hler: "+this.lernzyklusZ�hler);
		System.out.println();
	}
	
	public void save(String fileName){
		
		PrintWriter pw = null;
		try
		{
		  Writer fw = new FileWriter( fileName+".txt" );
		  Writer bw = new BufferedWriter( fw );
		  pw = new PrintWriter( bw );

		  
		  //------------------------
		  
		  double saveArray[][][]=new double[layerThickness.length-1][][];
			for(int i=0;i<this.layerThickness.length-1;i++){
				saveArray[i]=layer[i].save();
				for(int j=0;j<saveArray[i].length;j++){
					for(int k=0;k<saveArray[i][j].length;k++){
						pw.print(saveArray[i][j][k]+"~");
					}
					pw.println();
				}
				pw.println();
				pw.println();
			}
		  
			
			System.out.println("save");
		}
		catch ( IOException e ) {
		  System.err.println( "Error creating file!" );
		}
		finally {
		  if ( pw != null )
		    pw.close();
		}
		
		
		//Save as .NN
		double saveArray[][][]=new double[layerThickness.length-1][][];
		for(int i=0;i<this.layerThickness.length-1;i++){
			saveArray[i]=layer[i].save();
		}
	
		
		try {
			ObjectOutputStream outputStreamm = new ObjectOutputStream(new FileOutputStream(fileName+".NN"));
			outputStreamm.writeObject(saveArray);
		} 
		catch ( IOException e ) {
			System.err.println( "Error creating file!" );
		}
	}
	
	public void read(String fileName){
		//double readData[][][];
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(fileName+".NN"));
			double readData[][][] = (double[][][])inputStream.readObject();
			for(int i=0;i<readData.length;i++){
				for(int j=0;j<readData[i].length;j++){
					for(int k=0;k<readData[i][j].length-1;k++){
						//System.out.print(readData[i][j][k]);
					}	
				}	
			}
			int layerThicknessTest[]=new int[readData.length+1];
			for(int i=0;i<readData.length;i++){
				layerThicknessTest[i]=readData[i].length-1;
				//System.out.println("i:   "+(readData[i].length-1));
			}
			layerThicknessTest[layerThicknessTest.length-1]=readData[readData.length-1][0].length;
			
			boolean gleich=true;
			for(int i=0;i<layerThickness.length;i++){
				if(layerThickness[i]==layerThicknessTest[i]){}
				else{gleich=false;}
			}
			
			if(this.layerThickness.length==layerThicknessTest.length&&gleich){
				for(int i=0;i<this.layerThickness.length-1;i++){
					layer[i].load(readData[i]);
				}
			}else{
				System.out.println("Diese Datei ist nicht f�r das Netz bestimmt!");
			}
		} catch (IOException e) {
			System.err.println( "Error reading file!" );
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}


//-------------------------
//-----  --       --   ----
//----  -----   ------  ---
//---  ------- --------  --
//---  ----------------  --
//----  --------------  ---
//-----  ------------  ----
//------  ----------  -----
//-------  --------  ------
//--------  ------  -------
//---------  ----  --------
//----------  --  ---------
//----------- -  ----------
//inliebeanissa



//-------------------------