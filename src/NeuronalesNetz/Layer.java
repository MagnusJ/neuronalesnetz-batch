package NeuronalesNetz;
import java.util.Random;


public class Layer {
	private double[] layer;
	private double[] weights[];
	private double[] deltaWeigths[];
	private double[] errorBefore;
	private double df;
	
	private double[] input;
	

	public Layer(int layerLength, int inputLayerLength) {
		this.layer=new double[layerLength];
		
		this.input=new double[inputLayerLength+1];
		
		this.weights=new double[inputLayerLength+1][layerLength];
			
		this.deltaWeigths=new double[inputLayerLength+1][layerLength];
		this.errorBefore=new double[this.input.length];

		Random random = new Random();

		for(int i=0;i<layerLength;i++){
			for(int j=0;j<inputLayerLength+1;j++){
				this.weights[j][i]=random.nextDouble()*2-1;
			}
		}	
	}
	
	public double[] calculate(double[] in){		
		for(int i=0;i<in.length;i++){
			this.input[i]=in[i];
		}
		this.input[this.input.length-1]=1;		//BIAS
		
		for(int i=0;i<this.layer.length;i++){
			this.layer[i]=0;
			for(int j=0;j< this.input.length;j++){
				this.layer[i]+=this.input[j]*this.weights[j][i];
			}
			this.layer[i]=Math.tanh(this.layer[i]);
		}
		return this.layer;
	}
	
	public double[] learn(double[] mistake){
			
		//Delta WeightsIn---------------------------------------------------------------------------------------
			for(int i=0;i<this.layer.length;i++){
				df=1-this.layer[i]*this.layer[i];
				for(int j=0;j<this.input.length;j++){
					this.deltaWeigths[j][i]=this.input[j]*df*mistake[i];//*(this.weights[j][i]/this.weigthSum);this.lernFaktor*
				}
			}
		//-------------------------------------------------------------------------------------------------------		
	
		//Error Layer before
			for(int i=0; i<this.input.length;i++){
				this.errorBefore[i]=0;
				for(int j=0; j<layer.length;j++){
					this.errorBefore[i]+=(this.weights[i][j]*mistake[j]);
				}
			}
		//-------------------------------------------------------------------------------------------------------		
			
			
		//new weights--------------------------------------------------------------------------------------------		
		for(int i=0;i<this.input.length;i++){
				for(int j=0;j<this.layer.length;j++){
					this.weights[i][j]+=-this.deltaWeigths[i][j];
				}
			}
		//-------------------------------------------------------------------------------------------------------		
				
		return this.errorBefore;
	}
	
	public double[][] save(){
		return(this.weights);
	}

	public void load(double weights[][]){
		this.weights=weights;
	}
}
